FROM        amd64/debian:11
MAINTAINER  Jakob Blomer <jblomer@cern.ch>

ARG SFTNIGHT_UID=500
ARG SFTNIGHT_GID=500

RUN         apt-get -y update && apt-get -y upgrade
RUN         apt-get -y update && apt-get -y install              \
                                              autotools-dev      \
                                              cmake              \
                                              cpio               \
                                              debhelper          \
                                              devscripts         \
                                              gdb                \
                                              git                \
                                              golang             \
                                              libattr1-dev       \
                                              libcap-dev         \
                                              libfuse-dev        \
                                              libfuse3-dev       \
                                              libssl-dev         \
                                              libz-dev           \
                                              pbuilder           \
                                              pkg-config         \
                                              python-dev         \
                                              python-setuptools  \
                                              python3-dev        \
                                              python3-setuptools \
                                              unzip              \
                                              uuid-dev           \
                                              valgrind           \
                                              autoconf           \
                                              bison              \
                                              flex               \
                                              libhesiod-dev      \
                                              libkrb5-dev        \
                                              libldap2-dev       \
                                              libsasl2-dev       \
                                              libxml2-dev        \
                                              sssd-common

RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

USER sftnight

WORKDIR /home/sftnight
