FROM       gitlab-registry.cern.ch/cernvm/build-images/sles_x86_64:base12
MAINTAINER Jakob Blomer <jblomer@cern.ch>

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN        zypper -n update && zypper -n install    \
                                  cmake             \
                                  fuse-devel        \
                                  gcc               \
                                  gcc-c++           \
                                  gdb               \
                                  git               \
                                  libattr-devel     \
                                  libcap-devel      \
                                  libcurl-devel     \
                                  libopenssl-devel  \
                                  libuuid-devel     \
                                  make              \
                                  patch             \
                                  pkgconfig         \
                                  python-devel      \
                                  python-setuptools \
                                  rpmbuild          \
                                  tar               \
                                  unzip             \
                                  valgrind-devel    \
                                  zlib-devel

RUN        ln -s /usr/bin/ld.bfd /etc/alternatives/ld
RUN        ln -s /usr/bin/unzip-plain /etc/alternatives/unzip

# user and group sftnigh added in the base image
# RUN groupadd -g $SFTNIGHT_GID sftnight && \
# 	useradd -u $SFTNIGHT_UID -g sftnight sftnight

USER       sftnight
WORKDIR    /home/sftnight
