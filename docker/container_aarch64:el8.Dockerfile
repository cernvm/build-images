FROM 	    rockylinux:8
MAINTAINER  Jakob Blomer <jblomer@cern.ch>

# This two args should not be necessary, but the bug fixed by
# https://github.com/opencontainers/runc/pull/2086
# makes them necessary in our environment
ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN         yum -y update
RUN         yum -y install filesystem
RUN         yum -y install epel-release
RUN         yum -y install                     \
                       cmake                   \
                       curl-devel              \
                       fuse-devel              \
                       fuse3-devel             \
                       gcc-c++                 \
                       gdb                     \
                       git                     \
                       golang                  \
                       hardlink                \
                       libattr-devel           \
                       libcap-devel            \
                       libffi-devel            \
                       libuuid-devel           \
                       make                    \
                       openssl-devel           \
                       python2                 \
                       python2-devel           \
                       python3-devel           \
                       rpm-build               \
                       ruby-devel              \
                       selinux-policy-devel    \
                       selinux-policy-targeted \
                       which                   \
                       valgrind-devel          \
                       voms-devel              \
                       zlib-devel              \
                       lsb                     \
                       fuse3

RUN yum -y install autofs

RUN dnf config-manager --add-repo=https://download.docker.com/linux/centos/docker-ce.repo

# Don't include container-selinux and remove
# directories used by dnf that are just taking
# up space.
# RUN yum -y install buildah fuse-overlayfs --exclude container-selinux; rm -rf /var/cache /var/log/dnf* /var/log/yum.*
RUN yum -y install docker-ce buildah fuse-overlayfs --nobest --allowerasing --skip-broken
# Adjust storage.conf to enable Fuse storage.
# RUN sed -i -e 's|^#mount_program|mount_program|g' -e '/additionalimage.*/a "/var/lib/shared",' /etc/containers/storage.conf
# Set up environment variables to note that this is
# not starting with user namespace and default to
# isolate the filesystem with chroot.
ENV _BUILDAH_STARTED_IN_USERNS="" BUILDAH_ISOLATION=chroot
# Prepare for additional stores
# RUN mkdir -p /var/lib/shared/overlay-images /var/lib/shared/overlay-layers; touch /var/lib/shared/overlay-images/images.lock; touch /var/lib/shared/overlay-layers/layers.lock

ADD https://ecsft.cern.ch/dist/cvmfs/builddeps/busybox /usr/bin/busybox
RUN chmod 755 /usr/bin/busybox

# Picked up by $cvmfs-source/ci/build_package.sh
RUN echo "container" > /cvmfs-package-type

# Similarly to the node above.
# `adduser sftnight` should be sufficient, but the bug above requires
# this workaround
RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

USER        sftnight

WORKDIR     /home/sftnight
