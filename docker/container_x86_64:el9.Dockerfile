FROM 	    almalinux:9
MAINTAINER  Jakob Blomer <jblomer@cern.ch>

# This two args should not be necessary, but the bug fixed by
# https://github.com/opencontainers/runc/pull/2086
# makes them necessary in our environment
ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN         dnf -y update
RUN         dnf -y install filesystem
RUN         dnf -y install dnf-utils && dnf config-manager --set-enabled crb
RUN         dnf -y install epel-release
RUN         dnf -y install                     \
                       cmake                   \
                       curl-devel              \
                       fuse-devel              \
                       fuse3-devel             \
                       gcc-c++                 \
                       gdb                     \
                       git                     \
                       golang                  \
                       hardlink                \
                       libattr-devel           \
                       libcap-devel            \
                       libffi-devel            \
                       libuuid-devel           \
                       make                    \
                       openssl-devel           \
                       python3-devel           \
                       rpm-build               \
                       ruby-devel              \
                       selinux-policy-devel    \
                       selinux-policy-targeted \
                       systemd                 \
                       which                   \
                       valgrind-devel          \
                       voms-devel              \
                       zlib-devel              \
                       fuse3

RUN yum -y install autofs

RUN yum -y install buildah fuse-overlayfs

ADD https://ecsft.cern.ch/dist/cvmfs/builddeps/busybox /usr/bin/busybox
RUN chmod 755 /usr/bin/busybox

# Picked up by $cvmfs-source/ci/build_package.sh
RUN echo "container" > /cvmfs-package-type

# Similarly to the node above.
# `adduser sftnight` should be sufficient, but the bug above requires
# this workaround
RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

USER        sftnight

WORKDIR     /home/sftnight
