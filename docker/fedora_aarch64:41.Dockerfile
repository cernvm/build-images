FROM        arm64v8/fedora:41
MAINTAINER  Valentin Volkl <vavolkl@cern.ch>

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN         dnf -y update && dnf -y install                     \
                                        cmake                   \
                                        curl-devel              \
                                        fuse-devel              \
                                        fuse3-devel             \
                                        gcc                     \
                                        gcc-c++                 \
                                        gdb                     \
                                        git                     \
                                        golang                  \
                                        gridsite                \
                                        hardlink                \
                                        libattr-devel           \
                                        libcap-devel            \
                                        libuuid-devel           \
                                        make                    \
                                        nfs-utils               \
                                        openssl-devel           \
                                        openssl-devel-engine    \
                                        perl-IO-Interface       \
                                        python3-devel           \
                                        python-setuptools       \
                                        rpm-build               \
                                        selinux-policy-devel    \
                                        selinux-policy-targeted \
                                        sudo                    \
                                        tree                    \
                                        voms-devel              \
                                        which                   \
                                        valgrind-devel          \
                                        zlib-devel


RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

RUN GOPATH=/usr/local go install github.com/jstemmer/go-junit-report@latest

USER        sftnight
WORKDIR     /home/sftnight
