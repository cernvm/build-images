## See README.md, build.sh and ../build_on_docker.sh

FROM       scratch
MAINTAINER Jakob Blomer <jblomer@cern.ch>

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

ADD        sles12_x86_64.tar.gz /

RUN groupadd -g $SFTNIGHT_GID sftnight && \
	useradd -u $SFTNIGHT_UID -g sftnight sftnight

