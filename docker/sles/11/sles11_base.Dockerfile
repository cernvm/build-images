FROM       scratch

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

ADD        sles11_x86_64.tar.gz /

RUN groupadd -g $SFTNIGHT_GID sftnight && \
        useradd -u $SFTNIGHT_UID -g sftnight sftnight

