FROM        i386/ubuntu:18.04

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN         apt-get -y update && apt-get -y upgrade
RUN         apt-get -y install software-properties-common && add-apt-repository -y ppa:deadsnakes/ppa
RUN         apt-get -y update && apt-get -y install              \
                                              autotools-dev      \
                                              cmake              \
                                              cpio               \
                                              debhelper          \
                                              devscripts         \
                                              gdb                \
                                              git                \
                                              libattr1-dev       \
                                              libcap-dev         \
                                              libfuse-dev        \
                                              libssl-dev         \
                                              pkg-config         \
                                              python 3.10        \
                                              python-dev         \
                                              python-setuptools  \
                                              python3-dev        \
                                              python3-setuptools \
                                              unzip              \
                                              uuid-dev           \
                                              valgrind

RUN sed -i /etc/apt/sources.list -e 's/main$/main universe/'
RUN apt-get -y update && apt-get -y install \
  autoconf \
  bison \
  dh-systemd \
  flex \
  libhesiod-dev \
  libkrb5-dev \
  libldap2-dev \
  libsasl2-dev \
  libxml2-dev \
  sssd-common

RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

USER        sftnight
WORKDIR     /home/sftnight
