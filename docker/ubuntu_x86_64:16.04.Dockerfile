FROM        amd64/ubuntu:16.04
MAINTAINER  Jakob Blomer <jblomer@cern.ch>

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN         apt-get -y update && apt-get -y upgrade
RUN         apt-get -y update && apt-get -y install              \
                                              autotools-dev      \
                                              build-essential    \
                                              cmake              \
                                              cpio               \
                                              debhelper          \
                                              devscripts         \
                                              gdb                \
                                              gcc                \
                                              git                \
                                              golang             \
                                              libattr1-dev       \
                                              libcap-dev         \
                                              libffi-dev         \
                                              libfuse-dev        \
                                              libssl-dev         \
                                              pkg-config         \
                                              python-dev         \
                                              python-setuptools  \
                                              python3-dev        \
                                              python3-setuptools \
                                              ruby               \
                                              ruby-dev           \
                                              rubygems           \
                                              unzip              \
                                              uuid-dev           \
                                              valgrind

RUN apt-get -y update && apt-get -y install \
  doxygen \
  graphviz \
  gsfonts

RUN sed -i /etc/apt/sources.list -e 's/main$/main universe/'
RUN apt-get -y update && apt-get -y install \
  autoconf \
  bison \
  dh-systemd \
  flex \
  libhesiod-dev \
  libkrb5-dev \
  libldap2-dev \
  libsasl2-dev \
  libxml2-dev \
  sssd-common

RUN gem install --no-ri --no-rdoc fpm

RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

USER        sftnight
WORKDIR     /home/sftnight
