FROM 	   rockylinux:8
MAINTAINER Jakob Blomer <jblomer@cern.ch>

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN         dnf -y install epel-release && dnf -y update
RUN         dnf -y install                     \
                       filesystem              \
                       clang-analyzer          \
                       cmake                   \
                       curl-devel              \
                       fuse-devel              \
                       fuse3-devel             \
                       gcc-c++                 \
                       gdb                     \
                       git                     \
                       golang                  \
                       hardlink                \
                       libattr-devel           \
                       libcap-devel            \
                       libffi-devel            \
                       libuuid-devel           \
                       make                    \
                       openssl-devel           \
                       python2                 \
                       python2-devel           \
                       python3-devel           \
                       rpm-build               \
                       ruby-devel              \
                       selinux-policy-devel    \
                       selinux-policy-targeted \
                       which                   \
                       valgrind-devel          \
                       voms-devel              \
                       zlib-devel

RUN GOBIN=/usr/bin go install github.com/jstemmer/go-junit-report@latest

RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
     adduser  --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight
