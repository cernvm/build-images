FROM        arm64v8/ubuntu:22.04
MAINTAINER  Jakob Blomer <jblomer@cern.ch>

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

# necessary for tzdata
ENV DEBIAN_FRONTEND=noninteractive

RUN         apt-get -y update && apt-get -y upgrade
RUN         apt-get -y update && apt-get -y install              \
                                              autotools-dev      \
                                              build-essential    \
                                              cmake              \
                                              cpio               \
                                              debhelper          \
                                              devscripts         \
                                              gdb                \
                                              gcc                \
                                              git                \
                                              golang             \
                                              libattr1-dev       \
                                              libcap-dev         \
                                              libffi-dev         \
                                              libfuse-dev        \
                                              libfuse3-dev       \
                                              libssl-dev         \
                                              pkg-config         \
                                              python2-dev         \
                                              python-setuptools  \
                                              python3-dev        \
                                              python3-setuptools \
                                              ruby               \
                                              ruby-dev           \
                                              rubygems           \
                                              unzip              \
                                              uuid-dev           \
                                              valgrind           \
					      zlib1g-dev


RUN sed -i /etc/apt/sources.list -e 's/main$/main universe/'

RUN apt-get -y update && apt-get -y install \
  autoconf \
  bison \
  doxygen \
  graphviz \
  gsfonts \
  flex \
  libhesiod-dev \
  libkrb5-dev \
  libldap2-dev \
  libsasl2-dev \
  libxml2-dev \
  sssd-common

RUN GOBIN=/usr/bin go install github.com/jstemmer/go-junit-report@latest

RUN gem install fpm

RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

USER        sftnight
WORKDIR     /home/sftnight
