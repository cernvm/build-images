FROM 	  centos:7
MAINTAINER  Jakob Blomer <jblomer@cern.ch>

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN yum -y update;   \
	  yum -y install   \
		filesystem       \
		epel-release     \
		yum-plugin-ovl
# see https://github.com/CentOS/sig-cloud-instance-images/issues/15 for `yum-plugin-ovl`
RUN         yum -y install epel-release
RUN         yum -y install                     \
                       cmake                   \
                       curl-devel              \
                       fuse-devel              \
                       fuse3-devel             \
                       gcc                     \
                       gcc-c++                 \
                       git                     \
                       golang                  \
                       hardlink                \
                       libattr-devel           \
                       libcap-devel            \
                       libffi-devel            \
                       libuuid-devel           \
                       make                    \
                       openssl-devel           \
                       policycoreutils-python  \
                       python3                 \
                       python-devel            \
                       python-setuptools       \
                       rpm-build               \
                       ruby-devel              \
                       selinux-policy-devel    \
                       selinux-policy-targeted \
                       sysvinit-tools          \
                       which                   \
                       valgrind-devel          \
                       voms-devel              \
                       zlib-devel

RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
     adduser  --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

# Fix yum issues with Docker OverlayFS by installing this plugin
RUN yum -y install yum-plugin-ovl

RUN GOBIN=/usr/bin go get github.com/jstemmer/go-junit-report

#USER        sftnight
#WORKDIR     /home/sftnight
