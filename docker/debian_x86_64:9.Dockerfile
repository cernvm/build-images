FROM        amd64/debian:9
MAINTAINER  Jakob Blomer <jblomer@cern.ch>

ARG SFTNIGHT_UID=500
ARG SFTNIGHT_GID=500

RUN         apt-get -y update && apt-get -y upgrade
RUN         apt-get -y update && apt-get -y install              \
                                              autotools-dev      \
                                              cmake              \
                                              cpio               \
                                              debhelper          \
                                              devscripts         \
                                              gdb                \
                                              git                \
                                              golang             \
                                              libattr1-dev       \
                                              libcap-dev         \
                                              libfuse-dev        \
                                              libssl-dev         \
                                              libz-dev           \
                                              pbuilder           \
                                              pkg-config         \
                                              python-dev         \
                                              python-setuptools  \
                                              python3-dev        \
                                              python3-setuptools \
                                              unzip              \
                                              uuid-dev           \
                                              valgrind           \
                                              autoconf           \
                                              bison              \
                                              dh-systemd         \
                                              flex               \
                                              libhesiod-dev      \
                                              libkrb5-dev        \
                                              libldap2-dev       \
                                              libsasl2-dev       \
                                              libxml2-dev        \
                                              sssd-common        \
                                              fakechroot         \
                                              ruby               \
                                              ruby-dev           \
                                              rubygems

RUN gem install --no-ri --no-rdoc fpm

RUN  groupadd --gid $SFTNIGHT_GID sftnight && \
        adduser --uid $SFTNIGHT_UID --gid $SFTNIGHT_GID sftnight

USER sftnight
WORKDIR /home/sftnight
