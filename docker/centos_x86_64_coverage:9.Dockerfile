FROM 	      gitlab-registry.cern.ch/cernvm/build-images/centos_x86_64:9 
MAINTAINER  Valentin Volkl <vavolkl@cern.ch>

USER root
# pin gcovr version 5.0 due to 
# https://github.com/gcovr/gcovr/issues/583
# which in turn needs an older jinja2
RUN  pip3 install --prefix=/usr/local jinja2==3.0  gcovr==5.0

USER    sftnight
WORKDIR /home/sftnight
