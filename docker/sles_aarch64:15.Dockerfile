FROM       opensuse/leap
MAINTAINER Valentin Volkl <valentin.volkl@cern.ch>

RUN zypper --non-interactive update
RUN zypper --non-interactive install git curl tar rpm-build
RUN curl -O https://raw.githubusercontent.com/cvmfs/cvmfs/refs/heads/devel/packaging/rpm/cvmfs-universal.spec
RUN zypper -n install  $(rpmspec --parse cvmfs-universal.spec | grep BuildRequires | cut -d' ' -f2 | xargs)

ARG SFTNIGHT_UID=5005
ARG SFTNIGHT_GID=5005

RUN groupadd -g $SFTNIGHT_GID sftnight && \
  useradd -u $SFTNIGHT_UID -g sftnight sftnight

USER       sftnight
WORKDIR    /home/sftnight
